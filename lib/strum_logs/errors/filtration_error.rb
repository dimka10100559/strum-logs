# frozen_string_literal: true

module StrumLogs
  module Errors
    class Filtration < StandardError;
      def initialize(msg = "Unexpected error", exception_type="Filtration error")
        super msg
      end
    end
  end
end

